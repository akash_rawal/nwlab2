//Remote-controlled process, possibly running in another namespace

pub mod hd;
pub mod hub;

use std::collections::HashMap;
use std::future::Future;

use anyhow::Context;

use futures::future::LocalBoxFuture;
use tokio::io::{AsyncRead, AsyncReadExt, AsyncWrite, AsyncWriteExt};
use tokio::net::UnixStream;

//PDU functions
async fn read_pdu<R, T>(conn: &mut R) 
-> anyhow::Result<Option<T>> 
where R: AsyncRead + Unpin,
      for<'de> T: 'static + serde::Deserialize<'de>
{
    let maybe_pdu = async {
        let pdu_len = conn.read_u32().await?;
        let mut pdu = vec![0_u8; pdu_len as usize];
        if pdu_len > 0 {
            conn.read_exact(&mut pdu).await?;
        }

        Ok::<_, std::io::Error>(pdu)
    }.await;
    match maybe_pdu {
        Ok(pdu) => {
            let msg = rmp_serde::from_slice::<T>(&pdu)
                .context("Unable to deserialize request PDU")?;
            Ok(Some(msg))
        },
        Err(e) => {
            match e.kind() {
                std::io::ErrorKind::UnexpectedEof => Ok(None),
                _ => Err(anyhow::Error::new(e).context("Unable to read PDU")),
            }
        },
    }
}

async fn write_pdu<W, T> (conn: &mut W, msg: &T) -> anyhow::Result<()>
where W: AsyncWrite + Unpin,
      T: serde::Serialize
{
    let pdu = rmp_serde::to_vec(msg).context("Unable to serialize PDU")?;

    let c_write_pdu = "Unable to write PDU";
    conn.write_u32(pdu.len() as u32).await.context(c_write_pdu)?;
    conn.write_all(&pdu).await.context(c_write_pdu)
}

//Worker interface

pub trait Request: 'static {
    const NAME: &'static str;
    type Resp: 'static;
}

pub struct Worker {
    pub conn: UnixStream,
}

impl Worker {
    pub async fn req<R>(&mut self, req: &R) 
    -> anyhow::Result<<R as Request>::Resp>
    where R: Request + serde::Serialize,
          for<'de> <R as Request>::Resp: serde::Deserialize<'de> 
    {
        write_pdu(&mut self.conn, &String::from(R::NAME)).await?;
        write_pdu(&mut self.conn, req).await?;
        let resp: Option<Result<R::Resp, String>>
            = read_pdu(&mut self.conn).await?;
        let resp = resp.ok_or_else(|| {
            anyhow::Error::msg("Unexpected EOF when reading response")
        })?;
        resp.map_err(|e| anyhow::anyhow!("Error from worker process: {e}"))
    }
}

//Worker implementation
pub const HUB_SOCKET_VAR: &str = "HUB_SOCKET";

pub trait Handler:  {
    fn name(&self) -> &'static str;
    fn call<'a>(&'a self, conn: &'a mut UnixStream) -> LocalBoxFuture<'a, ()>;
}

pub trait IntoHandler<Any>:  {
    type HandlerType: Handler;
    fn into_handler(self) -> Self::HandlerType;
}

impl<T: Handler> IntoHandler<()> for T {
    type HandlerType = Self;
    fn into_handler(self) -> Self::HandlerType { self }
}

impl<Func, Req, Fut> Handler for (Func, std::marker::PhantomData<Req>)
where Func: Fn(Req) -> Fut,
      for<'de> Req: Request + serde::Deserialize<'de>,
      <Req as Request>::Resp: serde::Serialize,
      Fut: Future<Output = Result<<Req as Request>::Resp, String>>
{
    fn name(&self) -> &'static str {
        Req::NAME
    }

    fn call<'a>(&'a self, conn: &'a mut UnixStream) -> LocalBoxFuture<'a, ()> {
        Box::pin(async {
            let res = async {
                let req = read_pdu(conn).await?
                    .ok_or_else(|| anyhow::anyhow!("EOF when reading arguments"))?;

                let resp = (self.0)(req).await;
                write_pdu(conn, &resp).await?;

                Ok::<(), anyhow::Error>(())
            }.await;
            if let Err(e) = res {
                log::error!("Error handling request {}: {:?}", Req::NAME, e);
            }
        })
    }
}

impl<Func, Req, Fut> IntoHandler<Req> for Func
where Func: Fn(Req) -> Fut,
      for<'de> Req: Request + serde::Deserialize<'de>,
      <Req as Request>::Resp: serde::Serialize,
      Fut: Future<Output = Result<<Req as Request>::Resp, String>>
{
    type HandlerType = (Func, std::marker::PhantomData<Req>);
    fn into_handler(self) -> Self::HandlerType {
        (self, std::marker::PhantomData)
    }
}


#[derive(Default)]
pub struct HandlerSet<'a>(HashMap<&'static str, Box<dyn 'a + Handler>>);

impl<'a> HandlerSet<'a> {
    pub fn add<H, Any>(&mut self, handler: H) 
    where H: IntoHandler<Any>,
          <H as IntoHandler<Any>>::HandlerType: 'a
    {
        let handler = handler.into_handler();
        self.0.insert(handler.name(), Box::new(handler));
    }
}

pub async fn worker_impl(handlerset: HandlerSet<'_>) -> anyhow::Result<()> {
    let socket_addr = std::env::var(HUB_SOCKET_VAR)
        .with_context(|| format!("Unable to read variable {HUB_SOCKET_VAR}"))?;
    let mut conn = UnixStream::connect(socket_addr).await
        .context("Unable to connect to socket")?;

    while let Some(name) = read_pdu::<_, String>(&mut conn).await? {
        if let Some(handler) = handlerset.0.get(&name as &str) {
            handler.call(&mut conn).await;
        }
    }

    Ok(())
}

