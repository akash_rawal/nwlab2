//Handlers that can be called via worker

use std::fmt::Display;
use std::collections::HashMap;
use std::process::Stdio;
use std::sync::{Mutex, OnceLock};

use futures::future::LocalBoxFuture;

use tokio::io::AsyncReadExt;
use tokio::process::{Command, Child};

use super::{Worker, Request, HandlerSet};

#[derive(serde::Serialize, serde::Deserialize)]
pub struct GetPID;

impl Request for GetPID {
    const NAME: &'static str = "getpid";
    type Resp = u32;
}

async fn handle_getpid(_: GetPID) -> Result<u32, String> {
    Ok(std::process::id())
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct Args (pub Vec<String>);

impl<I> From<I> for Args 
where I: IntoIterator,
      I::Item: Into<String>
{
    fn from(value: I) -> Self {
        Self(value.into_iter().map(|x| x.into()).collect())
    }
}

impl Args {
    pub fn args<I>(mut self, l: I) -> Self
    where I: IntoIterator,
          I::Item: Into<String>
    {
        for i in l {
            self.0.push(i.into());
        }
        self
    }
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct Spawn {
    pub args: Args,
    pub stdout: bool,
    pub stderr: bool,
}

impl Request for Spawn {
    const NAME: &'static str = "spawn";
    type Resp = u32;
}

static CHILDREN: OnceLock<Mutex<HashMap<u32, Child>>> = OnceLock::new();

async fn handle_spawn(req: Spawn) -> Result<u32, String> {
    let mut children = CHILDREN.get_or_init(Default::default)
        .lock().unwrap();

    let mut args_iter = req.args.0.into_iter();

    let argv0 = args_iter.next().ok_or("First argument is required")?;
    let mut cmd = Command::new(argv0);
    cmd.args(args_iter);
    if req.stdout {
        cmd.stdout(Stdio::piped());
    }
    if req.stderr {
        cmd.stderr(Stdio::piped());
    }

    let child = cmd.spawn()
        .map_err(|e| format!("Unable to create child process: {e}"))?;
    let pid = child.id().ok_or("Unable to get child PID")?;

    children.insert(pid, child);
    Ok(pid)
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct Wait {
    pub pid: u32
}

#[derive(Default, serde::Serialize, serde::Deserialize)]
pub struct WaitResult {
    pub stdout: Vec<u8>,
    pub stderr: Vec<u8>,
    pub status: i32,
}

impl Request for Wait {
    const NAME: &'static str = "wait";
    type Resp = WaitResult;
}

async fn handle_wait(req: Wait) -> Result<WaitResult, String> {
    let mut child = {
        let mut children = CHILDREN.get_or_init(Default::default)
            .lock().unwrap();

        children.remove(&req.pid)
            .ok_or_else(|| format!("No child process with pid {}", req.pid))?
    };

    let mut res = WaitResult::default();
    let read_res: Result<_, std::io::Error> = futures::try_join!(async {
        if let Some(mut stdout) = child.stdout.take() {
            stdout.read_to_end(&mut res.stdout).await?;
        }
        Ok(())
    }, async {
        if let Some(mut stderr) = child.stderr.take() {
            stderr.read_to_end(&mut res.stderr).await?;
        }
        Ok(())
    });
    read_res.map_err(|e| format!("Unable to read from child: {e}"))?;

    let wait_result = child.wait().await
        .map_err(|e| format!("Unable to wait for child: {e}"))?;
    res.status = wait_result.code().unwrap_or(0);

    Ok(res)
}

pub fn make_handlerset() -> HandlerSet<'static> {
    let mut res = HandlerSet::default();

    res.add(handle_getpid);
    res.add(handle_spawn);
    res.add(handle_wait);
    
    res
}

//Convenient functions
pub trait WorkerExt {
    fn get_ref(&mut self) -> &mut Worker;

    fn sh<'a>(&'a mut self, cmd: impl 'a +  AsRef<str>)
    -> LocalBoxFuture<'a, anyhow::Result<()>> {
        Box::pin(async move {
            let w = self.get_ref();
            let args = Args::from(["sh", "-c", cmd.as_ref()]);
            let pid = w.req(&Spawn{args, stdout: false, stderr: false}).await?;
            let waitresult = w.req(&Wait{pid}).await?;
            if waitresult.status != 0 {
                return Err(anyhow::anyhow!(
                        "Command returned {} exit status", waitresult.status));
            }

            Ok(())
        })
    }

    fn connect<'a>(&'a mut self, 
                   a_name: impl 'a + Display, b_name: impl 'a + Display,
                   b: &'a mut Worker) -> LocalBoxFuture<'a, anyhow::Result<()>>
    {
        Box::pin(async move {
            let a = self.get_ref();

            let b_pid = b.req(&GetPID).await?;
            a.sh(format!("ip link add {a_name} type veth peer name temp"))
                .await?;
            a.sh(format!("ip link set temp netns {b_pid} name {b_name}"))
                .await?;

            Ok(())
        })
    }
}

impl WorkerExt for Worker {
    fn get_ref(&mut self) -> &mut Worker {
        self
    }
}
