//A central worker that spawns workers in separate network namespaces,
//thus creating a hub-and-spoke structure

use anyhow::Context;

use tokio::io::AsyncWriteExt;
use tokio::net::UnixListener;
use tokio::process::{Command, Child};

use super::{Worker, HUB_SOCKET_VAR};
use super::hd::{Args, Spawn, WorkerExt};

//Hub module that spawns workers in separate network namespaces
pub struct Hub {
    tempdir: tempdir::TempDir,
    listener: UnixListener,
    hub_process: Child,
    worker: Worker,
}

fn worker_args() -> anyhow::Result<Vec<String>> {
    let executable = assert_cmd::cargo::cargo_bin("worker")
        .as_os_str()
        .to_str().unwrap()
        .to_string();

    Ok(vec![executable])
}

impl Hub {
    pub async fn new() -> anyhow::Result<Self> {
        let tempdir = tempdir::TempDir::new("nwlab_hub_")
            .context("Unable to create temporary directory")?;

        //Create UDS for controlling the hub and namespaces
        let socket_name = tempdir.path().join("hub.socket");
        let listener = UnixListener::bind(&socket_name)
            .context("Cannot bind hub's controller socket")?;

        //Fork off the hub process in a network namespace
        let hub_process = Command::new("unshare")
            .args(["--user", "--map-root-user", "--pid"])
            .args(&worker_args()?)
            .env(HUB_SOCKET_VAR, &socket_name)
            .spawn().context("Cannot execute hub process")?;

        //Wait for hub process to connect to us
        let (conn, _) = listener.accept().await
            .context("Waiting for hub process to connect")?;

        Ok(Self {
            tempdir,
            listener,
            hub_process,
            worker: Worker{conn},
        })
    }

    pub async fn exit(mut self) -> anyhow::Result<()> {
        self.worker.conn.shutdown().await.context("Cannot close socket process")?;
        self.hub_process.wait().await.context("Cannot wait for hub process")?;

        drop(self.tempdir);

        Ok(())
    }

    pub async fn accept_worker(&mut self) -> anyhow::Result<Worker> {
        let (conn, _) = self.listener.accept().await
            .context("Waiting for namespace process to connect")?;

        Ok(Worker{conn})
    }

    pub async fn namespace(&mut self) -> anyhow::Result<Worker> {
        //Send command to hub to unshare new namespace
        let args = Args::from(["unshare", "--net", "--"])
            .args(worker_args()?);
        self.worker.req(&Spawn{args, stdout: false, stderr:false}).await?;

        //Wait for namespace process to connect to us
        let mut worker = self.accept_worker().await?;

        worker.sh("ip link set lo up").await?;

        Ok(worker)
    }
}


