use std::time::Duration;


#[tokio::main(flavor = "current_thread")]
async fn main() {
    let self_name = std::env::args().nth(1)
        .expect("$1 must be name of the router.");

    env_logger::Builder::from_default_env()
        .format_suffix(format!(" (node {self_name})\n").leak())
        .init();

    let conn_settings = nwlab2::router::conn::ConnSettings { 
        self_name, 
        interval: Duration::from_millis(500),
        nbr: nwlab2::router::nbr::NbrSettings { 
            connect_timeout: Duration::from_secs(5), 
            announce_timeout: Duration::from_secs(5),
            heartbeat_interval: Duration::from_secs(2),
        } 
    };
    nwlab2::router::worker::run_worker(conn_settings).await
        .expect("Worker failed");
}


