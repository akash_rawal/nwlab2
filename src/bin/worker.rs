use nwlab2::worker;

#[tokio::main(flavor = "current_thread")]
async fn main() {
    worker::worker_impl(worker::hd::make_handlerset())
                .await.unwrap()
}
