//Keyed futures: A set of futures that can be probed using a tag.

use std::collections::HashSet;
use std::hash::Hash;

use std::cell::RefCell;

use std::pin::Pin;
use std::task::{Poll, Context};
use std::future::Future;
use futures::stream::FuturesUnordered;
use futures::{Stream, StreamExt};

#[pin_project::pin_project]
struct KeyedFuture<Key, Fut> {
    key: Option<Key>,
    #[pin]
    fut: Fut,
}

impl<Key, Fut> Future for KeyedFuture<Key, Fut>
where 
    Fut: Future
{
    type Output = (Key, <Fut as Future>::Output);
    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let self_project = self.project();
        match self_project.fut.poll(cx) { 
            Poll::Ready(v) => Poll::Ready(( self_project.key.take().unwrap(), v)),
            Poll::Pending => Poll::Pending,
        }
    }
}

pub struct KeyedFuturesUnordered<Key, Fut> {
    contains: HashSet<Key>,
    futures: FuturesUnordered<KeyedFuture<Key, Fut>>,
}

impl<Key, Fut> Default for KeyedFuturesUnordered<Key, Fut>
where Key: Clone + Eq + Hash,
      Fut: Future
{
    fn default() -> Self {
        Self {
            contains: Default::default(),
            futures: FuturesUnordered::new(),
        }
    }
}

impl<Key, Fut> KeyedFuturesUnordered<Key, Fut>
where Key: Clone + Eq + Hash + Unpin,
      Fut: Future 
{
    pub fn push(&mut self, key: Key, fut: Fut) {
        assert!(!self.contains.contains(&key),
                "Cannot handle two tasks with same key");
        self.contains.insert(key.clone());
        self.futures.push(KeyedFuture{key: Some(key), fut});
    }

    pub fn contains(&self, key: &Key) -> bool {
        self.contains.contains(key)
    }
}

impl<Key, Fut> Stream for KeyedFuturesUnordered<Key, Fut>
where Key: Clone + Eq + Hash + Unpin,
      Fut: Future
{
    type Item = <Fut as Future>::Output;
    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) 
    -> Poll<Option<Self::Item>> {
        let self_mut = self.get_mut();
        let resp = self_mut.futures.poll_next_unpin(cx);
        match resp {
            Poll::Ready(Some((key, value))) => {
                self_mut.contains.remove(&key);
                Poll::Ready(Some(value))
            },
            Poll::Ready(None) => Poll::Ready(None),
            Poll::Pending => Poll::Pending,
        }
    }
}

pub struct RefCellStream<'a, S>(pub &'a RefCell<S>);

impl<'a, S> Stream for RefCellStream<'a, S>
where S: Stream + Unpin
{
    type Item = <S as Stream>::Item;
    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>)
    -> Poll<Option<Self::Item>> {
        let mut self_mut = self.0.borrow_mut();
        self_mut.poll_next_unpin(cx)
    }
}


#[cfg(test)]
mod test {
    use futures::StreamExt;

    use super::*;

    #[tokio::test]
    async fn after_push_a_future_can_be_tracked() {
        let mut set = KeyedFuturesUnordered::default();
        set.push(1, async{});
        assert!(set.contains(&1));
    }

    #[tokio::test]
    async fn after_future_exits_its_tag_is_removed() {
        let mut set = KeyedFuturesUnordered::default();
        set.push(1, async{});
        set.next().await;
        assert!(!set.contains(&1));
    }
}


