//Connectivity with neighbors, manages network interfaces and announces

use std::collections::HashMap;

use std::rc::Rc;
use std::cell::RefCell;
use std::pin::pin;

use anyhow::Context;

use futures::future::{Either, LocalBoxFuture};
use futures::{FutureExt, StreamExt};
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::sync::Notify;
use tokio::sync::mpsc::{channel, Sender, Receiver};

use std::time::Duration;

use std::net::{Ipv6Addr, SocketAddrV6};
use tokio::net::{TcpListener, TcpSocket, TcpStream, UdpSocket};

use crate::keyed_futures::{KeyedFuturesUnordered, RefCellStream};

use super::nbr::{Channel, Link, Nbr, NbrKey, NbrSettings, PDU};
use super::nbrdb::NbrDB;

pub const PORT: u16 = 21011;
pub const MULTICAST_GROUP: Ipv6Addr 
    = Ipv6Addr::new(0xff02, 0, 0, 0, 0, 0, 0, 0x2);
#[derive(Debug, serde::Serialize, serde::Deserialize, PartialEq, Eq, Hash, Clone)]
pub struct AnnouncePayload {
    pub name: String,
}

/*
 * TODO: We need to add authentication and encryption to the entire protocol
 */
struct TcpChannel {
    sender: Sender<PDU>,
    receiver: Receiver<PDU>,
    future: LocalBoxFuture<'static, std::io::Result<()>>,
    rank: u64,
}

impl TcpChannel {
    fn new(rank: u64, stream: TcpStream) -> Self {
        let (sender, mut fut_receiver) = channel(1); let (fut_sender, receiver) = channel(1);
        let future = async move {
            let (mut stream_read, mut stream_write) = tokio::io::split(stream);
            let read_fut = async {
                loop {
                    let size = match stream_read.read_u16().await { Err(e) => {
                        return Err(e);
                    }, Ok(v) => v};
                    let mut vec = vec![0_u8; size as usize];
                    stream_read.read_exact(&mut vec).await?;

                    let pdu = match rmp_serde::from_slice(&vec) { Err(e) => {
                        use std::io::{Error, ErrorKind};
                        return Err(Error::new(ErrorKind::InvalidData, e));
                    }, Ok(v) => v };

                    if let Err(e) = fut_sender.send(pdu).await {
                        use std::io::{Error, ErrorKind};
                        return Err(Error::new(ErrorKind::BrokenPipe, e));
                    }
                }
                //We get a type error if we remove this
                #[allow(unreachable_code)]
                Ok(())
            };
            let write_fut = async {
                while let Some(pdu) = fut_receiver.recv().await {
                    let data = match rmp_serde::to_vec(&pdu) { Err(e) => {
                        use std::io::{Error, ErrorKind};
                        return Err(Error::new(ErrorKind::InvalidData, e));
                    }, Ok(v) => v};
                    
                    stream_write.write_u16(data.len() as u16).await?;
                    stream_write.write_all(&data).await?;
                }
                Ok(())
            };

            futures::try_join!(read_fut, write_fut).map(|_| ())
        };
        Self {
            sender, 
            receiver,
            future: Box::pin(future),
            rank,
        }
    }
    
    fn set_rank(&mut self, rank: u64) {
        self.rank = rank;
    }
}

impl Channel for TcpChannel {
    fn rank(&self) -> u64 {
         self.rank
     } 
    fn send<'a>(&'a mut self, pdu: &'a PDU) 
        -> LocalBoxFuture<'a, std::io::Result<()>> {
        Box::pin(async move {
            let fut = pin!(self.sender.send(pdu.clone()));
            let res = futures::future::select(fut, &mut self.future).await;
            match res { Either::Left((result, _)) => {
                result.map_err(|e| {
                    use std::io::{Error, ErrorKind};
                    Error::new(ErrorKind::BrokenPipe, e)
                })
            }, Either::Right((error, _)) => error}
        })
    }
    fn recv(&mut self) -> LocalBoxFuture<'_, std::io::Result<PDU>> {
        let closed = || {
            use std::io::{Error, ErrorKind};
            Error::new(ErrorKind::BrokenPipe, "Channel closed")
        };

        Box::pin(async move {
            let fut = pin!(self.receiver.recv());
            let res = futures::future::select(fut, &mut self.future).await;
            match res { Either::Left((result, _)) => {
                result.ok_or_else(closed)
            }, Either::Right((error, _)) => match error {
                Ok(_) => { Err(closed()) },
                Err(e) => Err(e)
            }}
        })
    }
}

#[derive(Clone, Debug)]
pub struct ConnSettings {
    pub self_name: String,
    pub interval: Duration,
    pub nbr: NbrSettings,
} 

pub struct Interface {
    pub name: String,
    pub lladdr: Ipv6Addr,
    pub ifindex: u32,
}

#[derive(Debug)]
pub enum Error {
    Deserialize(serde_json::Error),
    Parse(std::net::AddrParseError),
}

struct Shared {
    ifaces: HashMap<String, Rc<Interface>>,
}


pub struct Interfaces<'a> {
    nbrdb: &'a NbrDB,
    settings: ConnSettings,
    shared: RefCell<Shared>,
    changed: Notify,
}

impl<'a> Interfaces<'a> {
    pub fn new(nbrdb: &'a NbrDB, settings: ConnSettings) -> Self {
        Self {
            nbrdb,
            settings,
            shared: RefCell::new(Shared {
                ifaces: Default::default(), 
            }),
            changed: Notify::new(),
        }
    }

    /*TODO: The implementation does not handle link state.
     * Currently link state must be up, but realistically we should 
     * track and handle link up/down.
     */
    pub fn add_from_json(&self, json: &str) -> Result<(), Error> {
        #[derive(serde::Deserialize)]
        struct IpAddrJson {
            ifindex: u32,
            ifname: String,
            addr_info: Vec<AddrInfoJson>
        }

        #[derive(serde::Deserialize)]
        struct AddrInfoJson {
            family: String, 
            local: String,
            scope: String,
        }

        let result: Vec<IpAddrJson> = serde_json::from_str(json)
            .map_err(Error::Deserialize)?;

        let mut changed = false;
        let mut shared = self.shared.borrow_mut();

        for el in result {
            if el.ifname == "lo" {
                continue;
            }
            let maybe_lladdr = el.addr_info.into_iter().find_map(|addr| {
                if addr.family == "inet6" && addr.scope == "link" {
                    return Some(addr.local);
                }
                None
            });
            if let Some(lladdr) = maybe_lladdr {
                shared.ifaces.insert(el.ifname.clone(), Rc::new(Interface {
                    ifindex: el.ifindex,            
                    name: el.ifname,
                    lladdr: lladdr.parse().map_err(Error::Parse)?,
                }));
                changed = true;
            }
        }

        if changed {
            self.changed.notify_one();
        }

        Ok(())
    }

    pub fn get(&self, name: &str) -> Option<Rc<Interface>> {
        self.shared.borrow().ifaces.get(name).cloned()
    }

    pub fn list(&self) -> Vec<Rc<Interface>> {
        self.shared.borrow().ifaces.values().cloned().collect()
    }

    async fn run_tcp_server(&self, iface: &Interface) {
        let addr = SocketAddrV6::new(iface.lladdr, PORT, 0, iface.ifindex);
        let listener = loop {
            match TcpListener::bind(addr).await { Err(e) => {
                log::error!("run_tcp_server: Cannot create TCP socket at {}: {:?}",
                            iface.lladdr, e);
                tokio::time::sleep(self.settings.interval).await;
            }, Ok(v) => { break v; }}
        };

        while let Ok((socket, remoteaddr)) = listener.accept().await {
            //Build channel
            let mut channel = TcpChannel::new(0, socket);

            let res = async {
                //Receive handshake and set the rank
                let first_pdu = channel.recv().await
                    .context("Cannot receive PDU")?;


                let (name, rank) = match first_pdu { 
                    PDU::Handshake{name, rank} => (name, rank),
                    x => return Err(anyhow::anyhow!("Invalid PDU {x:?}")),
                };
                channel.set_rank(rank);

                //Send response
                channel.send(&PDU::Handshake{name: name.clone(), rank}).await
                    .context("Cannot send response PDU")?;

                //Create neighbor if not exists and set/replace channel
                let nbrkey = NbrKey { name };
                let nbr = match self.nbrdb.get(&nbrkey) { None => {
                    let nbr = Nbr::new(nbrkey, self.settings.nbr.clone());
                    self.nbrdb.add_nbr(nbr.clone());
                    nbr
                }, Some(v) => v};
                nbr.set_channel(Box::new(channel));

                Ok(())
            }.await;

            if let Err(e) = res {
                log::error!("Unable to process new connection from {}: {}",
                            remoteaddr, e);
            }
        }
    }

    async fn connect_tcp_channel(
        &self, 
        iface: &Interface, 
        payload: &AnnouncePayload, 
        remote: Ipv6Addr) 
    {
        let fut = pin!(async {
            let res: anyhow::Result<()> = async {
                //Connect to TCP server on other side
                let socket = TcpSocket::new_v6()
                    .context("Unable to create TCP socket")?;
                socket.bind_device(Some(iface.name.as_bytes()))
                    .context("Unable to bind TCP socket")?;

                let stream = socket.connect((remote, PORT).into()).await
                    .context("Unable to connect to remote server")?;

                //Send handshake PDU
                let rank = rand::random();
                let mut channel = TcpChannel::new(rank, stream);
                let pdu = PDU::Handshake {
                    name: self.settings.self_name.clone(), 
                    rank 
                };
                channel.send(&pdu).await
                    .context("Unable to send handshake message")?;

                //Receive handshake
                let resp = channel.recv().await
                    .context("Unable to receive handshake")?;
                match resp { PDU::Handshake{rank: other_rank, ..} if other_rank == rank => {
                    //Create neighbor and set the channel
                    let nbrkey = NbrKey {
                        name: payload.name.clone(),
                    };
                    let nbr = match self.nbrdb.get(&nbrkey) { 
                        Some(x) => x,
                        None => Nbr::new(nbrkey, self.settings.nbr.clone()),
                    };
                    nbr.set_channel(Box::new(channel));
                    self.nbrdb.add_nbr(nbr.clone());
                    nbr.announce(Link {
                        local: iface.lladdr, 
                        remote
                    });
                }, x => {
                    return Err(anyhow::anyhow!("Invalid response PDU {x:?}"));
                }};


                Ok(())
            }.await;
            if let Err(e) = res {
                let lladdr = iface.lladdr;
                log::error!("TCP connect({lladdr} -> {remote}): failed: {e:?}");
            }
        });
        let timeout = pin!(tokio::time::sleep(self.settings.nbr.announce_timeout));
        let res = futures::future::select(fut, timeout).await;
        if let Either::Right(..) = res {
            let lladdr = iface.lladdr;
            log::error!("TCP connect({lladdr} -> {remote}): timeout");
        }
    }

    async fn run_udp_socket(&self, iface: &Interface) {
        //Logging context
        let dbg = iface.lladdr;

        let connecting = RefCell::new(KeyedFuturesUnordered::default());

        let handle_announce = |payload: AnnouncePayload, remote: Ipv6Addr| {
            let nbrkey = &NbrKey { name: payload.name.clone(), };
            if payload.name == self.settings.self_name {
                //Ignore our own packet
            } else if let Some(nbr) = self.nbrdb.get(nbrkey) {
                nbr.announce(Link { local: iface.lladdr, remote } );
            } else {
                let fut = async move {
                    self.connect_tcp_channel(iface, &payload, remote).await;
                };
                let mut connecting = connecting.borrow_mut();
                if ! connecting.contains(&remote) {
                    connecting.push(remote, fut);
                }
            }
        };

        let socket = loop {
            let res = async {
                let addr = SocketAddrV6::new(Ipv6Addr::UNSPECIFIED, PORT, 0, 0);
                let maybe_socket = socket2::Socket::new(
                    socket2::Domain::IPV6,
                    socket2::Type::DGRAM,
                    Some(socket2::Protocol::UDP)
                );
                let socket = match maybe_socket { Err(e) => {
                    return Err(e).context("Unable to create new socket");
                }, Ok(v) => v};
                log::info!("{dbg}: set_reuse_address");
                socket.set_reuse_address(true).context("setsockopt(SO_REUSEADDR)")?;
                log::info!("{dbg}: bind");
                socket.bind(&(addr.into())).context("bind(addr)")?;
                log::info!("{dbg}: bind_device");
                socket.bind_device(Some(iface.name.as_bytes()))
                    .context("Cannot bind socket to device")?;
                socket.join_multicast_v6(&MULTICAST_GROUP, iface.ifindex)
                    .context("Unable to join multicast group")?;
                socket.set_nonblocking(true).context("O_NONBLOCK")?;

                let socket = UdpSocket::from_std(std::net::UdpSocket::from(socket))
                    .context("Convert from socket2 socket to tokio socket")?;

                log::info!("{dbg} Finished");

                Ok(socket)
            }.await;
            match res { Err(e) => {
                log::error!("{dbg}: Unable to create socket: {e:?}");
                tokio::time::sleep(self.settings.interval).await;
            }, Ok(x) => break x };
        };
        loop {
            let fut1 = async { 
                //Periodically send an announce
                let packet = AnnouncePayload{
                    name: self.settings.self_name.clone(),
                };
                let bytes = rmp_serde::to_vec(&packet)
                    .expect("Cannot serialize announce packet");
                if let Err(e) 
                    = socket.send_to(&bytes, (MULTICAST_GROUP, PORT)).await {
                        log::error!("{dbg}: Unable to send multicast packet: {e}");
                    }

                //Read responses
                loop {
                    let res: Result<(), anyhow::Error> = async {
                        let mut buf = [0_u8; 1024];
                        let (size, src_addr) = socket.recv_from(&mut buf).await
                            .context("Cannot receive from socket")?;
                        let bytes = &buf[0..size];

                        let msg: AnnouncePayload = rmp_serde::from_slice(bytes)
                            .context("Unable to deserialize packet")?;
                        let src_addr = match src_addr {
                            std::net::SocketAddr::V6(v6addr) => *v6addr.ip(),
                            x => Err(anyhow::anyhow!("Packet received from \
                                                     non-ipv6 address {x}"))?,
                        };

                        handle_announce(msg, src_addr);
                        Ok(())
                    }.await;
                    if let Err(e) = res {
                        log::error!("{dbg}: Unable to recv from socket: {e}");
                    }
                }
            };
            let fut2 = async {
                RefCellStream(&connecting).collect::<()>().await;
                std::future::pending::<()>().await;
            };
            let fut3 = tokio::time::sleep(self.settings.interval);
            futures::select!{
                _ = pin!(fut1.fuse()) => (),
                _ = pin!(fut2.fuse()) => (),
                _ = pin!(fut3.fuse()) => (),
            }
        }
    }


    async fn run_interface(&self, iface: Rc<Interface>) {
        futures::join!(
            self.run_udp_socket(&iface),
            self.run_tcp_server(&iface),
        );
    }


    pub async fn run(&self) {
        let mut futureset = KeyedFuturesUnordered::default();

        loop {
            let fut1 = async {
                //Syncrhonize from shared
                {
                    let shared = self.shared.borrow();
                    for (name, iface) in &shared.ifaces {
                        if !futureset.contains(name) {
                            let name = name.clone();
                            let iface = iface.clone();
                            futureset.push(name, self.run_interface(iface));
                        }
                    }
                }

                while futureset.next().await.is_some() { }

                //We have no interfaces left :(
                futures::future::pending::<()>().await;
            };
            let fut2 = self.changed.notified();
            futures::future::select(pin!(fut1), pin!(fut2)).await;
        }
    }
}


