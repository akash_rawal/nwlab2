//Management of one particular neighbor.

use std::collections::HashMap;

use std::pin::pin;
use std::rc::Rc;
use std::cell::RefCell;
use tokio::sync::Notify;

use std::net::Ipv6Addr;

use futures::future::{LocalBoxFuture, FutureExt};

use tokio::time::{Instant, Duration};

//Data definitions
#[derive(Hash, Eq, PartialEq, Clone)]
pub struct NbrKey {
    pub name: String, 
}

#[derive(Hash, Eq, Clone, Debug, PartialEq, 
         serde::Serialize, serde::Deserialize)]
pub struct Link {
    pub local: Ipv6Addr,
    pub remote: Ipv6Addr,
}

#[derive(Clone, Debug)]
pub struct LinkInfo {
    last_seen: Instant,
}

#[derive(Clone, Debug, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub enum PDU {
    Handshake{name: String, rank: u64},
    Heartbeat{announces: Vec<Link>},
}

pub trait Channel {
    fn rank(&self) -> u64;
    /*
     * Drop safety: 
     * There should be no data corruption if returned future is dropped. 
     * Once send is called, PDU has to be sent in whole.
     * Once recv is called, partially received PDU should be completed.
     */
    fn send<'a>(&'a mut self, pdu: &'a PDU) 
        -> LocalBoxFuture<'a, std::io::Result<()>>;
    fn recv(&mut self) -> LocalBoxFuture<'_, std::io::Result<PDU>>;
}

//Implementation

pub struct NbrMsg {
    pub channel: Option<Box<dyn Channel>>,
    pub announces: HashMap<Link, LinkInfo>,
    pub links: HashMap<Link, LinkInfo>,
}

#[derive(Clone, Debug)]
pub struct NbrSettings {
    pub connect_timeout: Duration,
    pub announce_timeout: Duration, 
    pub heartbeat_interval: Duration,
}

pub struct Nbr {
    key: NbrKey, 
    settings: NbrSettings,
    msg: RefCell<NbrMsg>,
    newmsg: Notify,
}

impl Nbr {
    pub fn new(key: NbrKey, settings: NbrSettings) -> Rc<Self> {
        Rc::new(Self {
            key, settings,
            msg: RefCell::new(NbrMsg {
                channel: None,
                announces: Default::default(),
                links: Default::default(),
            }),
            newmsg: Notify::new(),
        })
    }

    fn trim_announces(&self, map: &mut HashMap<Link, LinkInfo>) {
        let now = Instant::now();
        map.retain(|_, link_info| {
            now - link_info.last_seen <= self.settings.announce_timeout
        });
    }

    fn set_channel_fn(target: &mut Option<Box<dyn Channel>>, 
                       new_channel: Box<dyn Channel>) {
        if let Some(ref cur_channel) = target {
            if cur_channel.rank() < new_channel.rank() {
                *target = Some(new_channel);
            }
        } else {
            *target = Some(new_channel);
        }
    }

    pub fn key(&self) -> &NbrKey {
        &self.key
    }

    pub fn announce(&self, announce: Link) {
        self.msg.borrow_mut().announces.insert(announce, LinkInfo {
            last_seen: Instant::now(),
        });
        self.newmsg.notify_one();
    }

    pub fn links(&self) -> Vec<Link> {
        let mut msg = self.msg.borrow_mut();
        self.trim_announces(&mut msg.links);
        msg.links.keys().cloned().collect()
    }

    pub fn set_channel(&self, channel: Box<dyn Channel>) {
        let mut msg = self.msg.borrow_mut();
        Self::set_channel_fn(&mut msg.channel, channel);
        drop(msg);
        self.newmsg.notify_one();
    }

    fn handle_msg(&self, pdu: PDU, announces: &HashMap<Link, LinkInfo>) {
        let dbg = &self.key.name;
        let mut msg = self.msg.borrow_mut();
        match pdu { PDU::Heartbeat { announces: remote_announces } => {
            for link in &remote_announces {
                let local_link = Link {
                    local: link.remote,
                    remote: link.local,
                };

                if let Some(value) = announces.get(&local_link) {
                    log::info!("{dbg}: Adding link {local_link:?}, {value:?}");
                    msg.links.insert(local_link, value.clone());
                }
            }
            self.trim_announces(&mut msg.links);
        }, x => {
            //TODO: Other kinds of PDUs
            log::error!("Cannot handle PDU: {x:?}");
        }}
    }

    pub async fn run(self: Rc<Self>) {
        let mut maybe_channel: Option<Box<dyn Channel>> = None;
        let mut announces: HashMap<Link, LinkInfo> = Default::default();

        let mut hb_timer = tokio::time::interval(self.settings.heartbeat_interval);
        hb_timer.set_missed_tick_behavior(tokio::time::MissedTickBehavior::Delay);

        let mut alive = true;
        while alive {
            //Transfer from msg to state
            {
                let mut msg = self.msg.borrow_mut();
                if let Some(new_channel) = msg.channel.take() {
                    Self::set_channel_fn(&mut maybe_channel, new_channel);
                }
                announces.extend(msg.announces.drain());
            }
            //Main logic
            let fut1 = self.newmsg.notified();
            let fut2 = async {
                let result: std::io::Result<()> = async {
                    if let Some(ref mut channel) = maybe_channel {
                        //Handle communications with the neighbor
                        let res : Option<PDU> = {
                            let mut fut1 = pin!(hb_timer.tick().fuse());
                            let mut fut2 = pin!(channel.recv().fuse());
                            futures::select!{
                                _ = fut1 => None,
                                result = fut2 => Some(result?),
                            }
                        };
                        self.trim_announces(&mut announces);
                        if let Some(pdu) = res {
                            self.handle_msg(pdu, &announces)
                        } else {
                            let msg = PDU::Heartbeat { 
                                announces: announces.keys().cloned().collect(),
                            };
                            channel.send(&msg).await?;
                        }

                    } else {
                        //Wait for channel
                        tokio::time::sleep(self.settings.connect_timeout).await;
                        //Timeout, we're dead.
                        alive = false;
                    }

                    Ok(())
                }.await;
                if let Err(e) = result {
                    log::error!("Channel encountered error: {e}");
                    maybe_channel = None;
                }
            };
            futures::future::select(pin!(fut1), pin!(fut2)).await;
        }
    }
}

