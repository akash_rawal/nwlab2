use std::net::Ipv6Addr;
use std::process::Stdio;

use tokio::process::Command;

use crate::worker::{worker_impl, HandlerSet, Request};

use super::conn::{ConnSettings, Interfaces};
use super::nbr::NbrKey;
use super::nbrdb::NbrDB;

#[derive(serde::Serialize, serde::Deserialize)]
pub struct SetupInterface {
    pub name: String,
}

impl Request for SetupInterface {
    const NAME: &'static str = "setup_interface";
    type Resp = ();
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct GetNextHop {
    pub name: String,
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct NextHopResp {
    pub local: Ipv6Addr,
    pub remote: Ipv6Addr,
    pub iface: String,
}

impl Request for GetNextHop {
    const NAME: &'static str = "get_next_hop";
    type Resp = NextHopResp;
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct GetLinks {
    pub name: String,
}

impl Request for GetLinks {
    const NAME: &'static str = "get_links";
    type Resp = Vec<NextHopResp>;
}

pub async fn run_worker(conn_settings: ConnSettings) -> anyhow::Result<()> {
    let nbrdb = NbrDB::new();

    let ifaces = Interfaces::new(&nbrdb, conn_settings.clone());

    let mut handlerset = HandlerSet::default();
    handlerset.add(|req: SetupInterface| {
        let ifaces = &ifaces;
        async move {
            let cmd = Command::new("ip")
                .args(["-j", "addr", "show", "dev", &req.name])
                .stdout(Stdio::piped())
                .output().await
                .map_err(|e| format!("spawn: {e}"))?;

            let output = String::from_utf8(cmd.stdout)
                .map_err(|e| format!("Invalid output: {e}"))?;

            ifaces.add_from_json(&output)
                .map_err(|e| format!("Cannot add interface from json: {e:?}"))?;
            
            Ok(())
        }
    });
    handlerset.add(|req: GetNextHop| {
        let nbrdb = &nbrdb;
        let ifaces = &ifaces;
        async move {
            let nbrkey = NbrKey {
                name: req.name,
            };
            let nbr = nbrdb.get(&nbrkey).ok_or("Neighbor does not exist")?;
            let link = nbr.links().into_iter().next()
                .ok_or("Neighbor exists, but is unreachable")?;
            Ok(NextHopResp {
                local: link.local,
                remote: link.remote,
                iface: ifaces.list().into_iter()
                    .find_map(|iface| (iface.lladdr == link.local)
                              .then_some(iface.name.clone()))
                    .ok_or_else(|| format!("Stray interface {}", link.local))?,
            })
        }
    });
    handlerset.add(|req: GetLinks| {
        let nbrdb = &nbrdb;
        let ifaces = &ifaces;
        async move {
            let nbrkey = NbrKey {
                name: req.name,
            };
            let nbr = nbrdb.get(&nbrkey).ok_or("Neighbor does not exist")?;
            let mut resp = Vec::new();
            for link in nbr.links() {
                resp.push(NextHopResp {
                    local: link.local,
                    remote: link.remote,
                    iface: ifaces.list().into_iter()
                        .find_map(|iface| (iface.lladdr == link.local)
                                  .then_some(iface.name.clone()))
                        .ok_or_else(|| format!("Stray interface {}", link.local))?,
                });
            };
            Ok(resp)
        }
    });

    let fut1 = worker_impl(handlerset);
    let fut2 = ifaces.run();
    let fut3 = nbrdb.run();
    futures::join!(fut1, fut2, fut3).0
}


