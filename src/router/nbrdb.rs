//Neighbor database that maintains neighbors and how to reach them
//using notifications from other modules

use std::collections::HashMap;

use std::cell::RefCell;
use std::rc::Rc;
use std::pin::pin;

use futures::stream::StreamExt;
use crate::keyed_futures::KeyedFuturesUnordered;
use tokio::sync::Notify;

use super::nbr::*;

struct Shared {
    nbr_map: HashMap<NbrKey, Rc<Nbr>>,
}

pub struct NbrDB {
    shared: RefCell<Shared>, 
    notify: Notify,
}

impl Default for NbrDB {
    fn default() -> Self {
        Self::new()
    }
}

impl NbrDB {
    pub fn new() -> Self {
        Self {
            shared: RefCell::new(Shared {
                nbr_map: Default::default(),
            }),
            notify: Notify::new(),
        }
    }

    pub fn add_nbr(&self, nbr: Rc<Nbr>) {
        log::info!("Adding neighbor {}", nbr.key().name);

        let mut msg = self.shared.borrow_mut();

        msg.nbr_map.insert(nbr.key().clone(), nbr);
        self.notify.notify_one();
    }

    pub fn get(&self, key: &NbrKey) -> Option<Rc<Nbr>> {
        let shared = self.shared.borrow();

        shared.nbr_map.get(key).cloned()
    }

    pub async fn run(&self) {
        let mut nbr_runner = KeyedFuturesUnordered::default();

        loop {
            {
                let shared = self.shared.borrow();
                for nbr in shared.nbr_map.values() {
                    let fut = {
                        let nbr = nbr.clone();
                        async move {
                            nbr.clone().run().await;
                            log::info!("Removing neighbor {}", nbr.key().name);
                            self.shared.borrow_mut().nbr_map.remove(nbr.key());
                        }
                    };
                    if ! nbr_runner.contains(&nbr.key().name) {
                        nbr_runner.push(nbr.key().name.clone(), fut)
                    }
                }
            }

            let fut1 = async {
                while nbr_runner.next().await.is_some() {};
                futures::future::pending::<()>().await;
            };
            let fut2 = self.notify.notified();
            futures::future::select(pin!(fut1), pin!(fut2)).await;
        }
    }
}
