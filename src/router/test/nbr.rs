use std::cell::Cell;
use std::pin::pin;

use std::future::Future;

use std::rc::Rc;
use std::time::Duration;

use futures::future::LocalBoxFuture;

use tokio::sync::mpsc::{channel, Sender, Receiver};
use tokio::time::Instant;

use super::super::nbr::*;

struct PipeChannel(u64, Sender<PDU>, Receiver<PDU>);
impl Channel for PipeChannel {
    fn rank(&self) -> u64 {
         self.0
     } 
    fn send<'a>(&'a mut self, pdu: &'a PDU) 
            -> LocalBoxFuture<'a, std::io::Result<()>> {
        Box::pin(async move {
            self.1.send(pdu.clone()).await.unwrap();
            Ok(())
        })
    }
    fn recv(&mut self) -> LocalBoxFuture<'_, std::io::Result<PDU>> {
        use std::io::{Error, ErrorKind};
        Box::pin(async move {
            self.2.recv().await.ok_or_else(|| {
                Error::new(ErrorKind::UnexpectedEof, "Channel closed")
            })
        })
    }
}

fn pdu_repr(pdu: PDU) -> String {
    use std::fmt::Write;

    let mut res = String::new();
    match pdu {
        PDU::Handshake{name, rank} => { write!(res, "Handshake({name}, {rank})").unwrap(); },
        PDU::Heartbeat { announces } => {
            write!(res, "Heartbeat(").unwrap();
            let mut announces = announces.into_iter()
                .map(|Link{local, remote}| format!("{local} - {remote}"))
                .collect::<Vec<_>>();
            announces.sort();
            for el in announces {
                write!(res, "{el}, ").unwrap(); 
            }
            write!(res, ")").unwrap();
        }
    }
    res
}

struct TestFacade {
    nbr: Rc<Nbr>,
    deleted: Cell<bool>,
}

impl TestFacade {
    pub fn make() -> Self {
        let settings = NbrSettings {
            connect_timeout: Duration::from_secs(10),
            announce_timeout: Duration::from_secs(15), 
            heartbeat_interval: Duration::from_secs(10),
        };
        Self {
            nbr: Nbr::new(NbrKey{name: "localhost".into()}, settings),
            deleted: Cell::new(false),
        }
    }

    pub async fn run_while(&self, fut: impl Future) {
        futures::future::select(pin!(fut), 
            futures::future::select(pin!(async {
                self.nbr.clone().run().await;
                self.deleted.set(true);
            }), pin!(async {
                tokio::time::sleep(Duration::from_secs(300)).await;
                panic!("Test timed out");
            }))
        ).await;
    }

    pub fn status(&self) -> &str {
        if self.deleted.get() {
            "delete"
        } else {
            "stay"
        }
    }

    pub fn set_channel(&self) -> PipeChannel {
        let outgoing = channel(1);
        let incoming = channel(1);
        let local = PipeChannel(0, outgoing.0, incoming.1);
        let remote = PipeChannel(0, incoming.0, outgoing.1);
        self.nbr.set_channel(Box::new(local));
        remote
    }

    pub fn announce(&self, local: &str, remote: &str) {
        self.nbr.announce(Link { 
            local: local.parse().unwrap(), 
            remote: remote.parse().unwrap(), 
        });
    }

    pub fn links(&self) -> Vec<String> {
        let mut links: Vec<String> = self.nbr.links().into_iter()
            .map(|l| format!("{} - {}", l.local, l.remote))
            .collect();
        links.sort();
        links
    }
}

#[tokio::test(start_paused = true)]
async fn on_no_announces_till_connect_timeout_expire() {
    let f = TestFacade::make();

    f.run_while(async {
        tokio::time::sleep(Duration::from_secs(11)).await;
        unreachable!();
    }).await;

    assert_eq!(f.status(), "delete");
}

#[tokio::test(start_paused = true)]
async fn on_recv_announce_try_handshake_with_nbr() {
    let f = TestFacade::make();
    f.run_while(async {
        let mut channel = f.set_channel();
        let msg = channel.recv().await.unwrap();
        assert_eq!(pdu_repr(msg), "Heartbeat()");
    }).await;
}

#[tokio::test(start_paused = true)]
async fn heartbeats_are_spaced_evenly_every_interval() {
    let f = TestFacade::make();
    f.run_while(async {
        let mut channel = f.set_channel();
        let time = Instant::now();

        channel.recv().await.unwrap();
        assert_eq!(Instant::now() - time, Duration::ZERO);

        channel.recv().await.unwrap();
        assert_eq!(Instant::now() - time, Duration::from_secs(10));

        channel.recv().await.unwrap();
        assert_eq!(Instant::now() - time, Duration::from_secs(20));
    }).await;
}

#[tokio::test(start_paused = true)]
async fn on_recv_more_announce_verify_announces() {
    let f = TestFacade::make();
    f.run_while(async {
        let mut channel = f.set_channel();
        f.announce("fe80::1", "fe80::2");
        let msg = channel.recv().await.unwrap();
        assert_eq!(pdu_repr(msg), "Heartbeat(fe80::1 - fe80::2, )");
    }).await;
}



#[tokio::test(start_paused = true)]
async fn announces_expire_after_announce_expiry_time() {
    let f = TestFacade::make();
    f.run_while(async {
        let mut channel = f.set_channel();
        f.announce("fe80::1", "fe80::2");
        let msg = channel.recv().await.unwrap();
        assert_eq!(pdu_repr(msg), "Heartbeat(fe80::1 - fe80::2, )");
        let msg = channel.recv().await.unwrap();
        assert_eq!(pdu_repr(msg), "Heartbeat(fe80::1 - fe80::2, )");
        let msg = channel.recv().await.unwrap();
        assert_eq!(pdu_repr(msg), "Heartbeat()");
    }).await;
}

#[tokio::test(start_paused = true)]
async fn nbr_sends_same_announces_means_bidirectional_connectivity_verified() {
    let f = TestFacade::make();
    f.run_while(async {
        let mut channel = f.set_channel();
        f.announce("fe80::1", "fe80::2");
        let mut msg = channel.recv().await.unwrap();
        assert_eq!(pdu_repr(msg.clone()), "Heartbeat(fe80::1 - fe80::2, )");
        match msg { PDU::Heartbeat { announces } => {
            msg = PDU::Heartbeat{
                announces: announces.into_iter()
                    .map(|l| Link{local: l.remote, remote: l.local})
                    .collect()
            };
        }, x => panic!("Invalid PDU {x:?}")}
        channel.send(&msg).await.unwrap();
        tokio::time::sleep(Duration::from_secs(5)).await;
        assert_eq!(f.links()[0], "fe80::1 - fe80::2");
    }).await;
}

