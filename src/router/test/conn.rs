
use std::cell::{Ref, RefCell};
use std::time::Duration;

use crate::router::nbr::NbrSettings;
use crate::router::nbrdb::NbrDB;

use super::super::conn::*;

struct TestFacade<'a> {
    nbrdb: NbrDB,
    ifaces: RefCell<Option<Interfaces<'a>>>,
}
impl <'a> TestFacade<'a> {
    fn create() -> Self { 
        let nbrdb = NbrDB::new();

        Self {
            nbrdb,
            ifaces: RefCell::new(None),
        }
    }
    fn ifaces(&'a self) -> Ref<'a, Interfaces> {
        Ref::map(self.ifaces.borrow(), |x| x.as_ref().unwrap() )
    }
    fn init(&'a self) {
        let settings = ConnSettings {
            self_name: "a".into(),
            interval: Duration::from_secs(5),
            nbr: NbrSettings {
                connect_timeout: Duration::ZERO,
                announce_timeout: Duration::ZERO,
                heartbeat_interval: Duration::ZERO,
            }
        };
        *self.ifaces.borrow_mut() = Some(Interfaces::new(&self.nbrdb, settings));
    }
    fn add_from_json(&'a self, json: &str) -> Vec<String> {
        self.ifaces().add_from_json(json).unwrap();
        let mut res: Vec<String>
         = self.ifaces().list().into_iter().map(|iface| iface.name.to_owned()).collect();
        res.sort();
        res
    }
    fn get_addr(&'a self, name: &str) -> String {
        self.ifaces().get(name).unwrap().lladdr.to_string()
    }
}


#[test]
fn can_add_iface_from_json() {
    let f = TestFacade::create();
    f.init();

    assert_eq!(f.add_from_json(JSON), ["eth0", "eth1"]);
    assert_eq!(f.get_addr("eth0"), "fe80::14d4:50ff:fed2:3b97");
}

//Data
const JSON: &str = r#"[ {
        "ifindex": 1, 
        "ifname": "lo",
        "flags": [ "LOOPBACK" ],
        "mtu": 65536,
        "qdisc": "noop",
        "operstate": "DOWN",
        "group": "default",
        "txqlen": 1000,
        "link_type": "loopback",
        "address": "00:00:00:00:00:00",
        "broadcast": "00:00:00:00:00:00",
        "addr_info": [ ]
    },{
        "ifindex": 2,
        "link": "eth0",
        "ifname": "eth1",
        "flags": [ "BROADCAST","MULTICAST","UP","LOWER_UP" ],
        "mtu": 1500,
        "qdisc": "noqueue",
        "operstate": "UP",
        "group": "default",
        "txqlen": 1000,
        "link_type": "ether",
        "address": "aa:e8:4f:1b:fb:06",
        "broadcast": "ff:ff:ff:ff:ff:ff",
        "addr_info": [ {
                "family": "inet6",
                "local": "fe80::a8e8:4fff:fe1b:fb06",
                "prefixlen": 64,
                "scope": "link",
                "protocol": "kernel_ll",
                "valid_life_time": 4294967295,
                "preferred_life_time": 4294967295
            } ]
    },{
        "ifindex": 3,
        "link": "eth1",
        "ifname": "eth0",
        "flags": [ "BROADCAST","MULTICAST","UP","LOWER_UP" ],
        "mtu": 1500,
        "qdisc": "noqueue",
        "operstate": "UP",
        "group": "default",
        "txqlen": 1000,
        "link_type": "ether",
        "address": "16:d4:50:d2:3b:97",
        "broadcast": "ff:ff:ff:ff:ff:ff",
        "addr_info": [ {
                "family": "inet6",
                "local": "fe80::14d4:50ff:fed2:3b97",
                "prefixlen": 64,
                "scope": "link",
                "protocol": "kernel_ll",
                "valid_life_time": 4294967295,
                "preferred_life_time": 4294967295
            } ]
    } ]
"#;

