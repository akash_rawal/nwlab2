
use std::time::Duration;

use std::rc::Rc;

use std::future::Future;
use std::pin::pin;

use super::super::nbrdb::*;
use super::super::nbr::*;


struct TestFacade {
    db: NbrDB,
}
impl TestFacade {
    fn make() -> Self {
        Self {
            db: NbrDB::new(),
        }
    }
    fn make_nbr(&self, name: &str) -> Rc<Nbr> {
        let key = NbrKey {
            name: name.into(),
        };
        let settings = NbrSettings {
            connect_timeout: Duration::from_secs(10),
            announce_timeout: Duration::from_secs(15), 
            heartbeat_interval: Duration::from_secs(10),
        };
        Nbr::new(key, settings)
    }
    fn add_nbr(&self, nbr: Rc<Nbr>) {
        self.db.add_nbr(nbr)
    }
    fn get(&self, name: &str) -> Option<String> {
        let key = NbrKey {
            name: name.into(),
        };
        self.db.get(&key).map(|v| v.key().name.clone())
    }
    async fn run_while(&self, fut: impl Future) {
        futures::future::select(pin!(fut), 
            futures::future::select(pin!(async {
                self.db.run().await;
                panic!("Future exited");
            }), pin!(async {
                tokio::time::sleep(Duration::from_secs(300)).await;
                panic!("Test timed out");
            }))
        ).await;
    }
}

#[test]
fn an_added_nbr_is_retrivable_via_its_key() {
    let f = TestFacade::make();
    let nbr = f.make_nbr("a");
    f.add_nbr(nbr.clone());
    assert!(f.get("a").is_some());
}

#[tokio::test(start_paused = true)]
async fn unconnected_nbr_gets_removed_after_its_future_exits() {
    let f = TestFacade::make();
    f.run_while(async {
        let nbr = f.make_nbr("a");
        f.add_nbr(nbr.clone());
        tokio::time::sleep(Duration::from_secs(9)).await;    
        assert!(f.get("a").is_some());
        tokio::time::sleep(Duration::from_secs(2)).await;    
        assert!(f.get("a").is_none());
    }).await;
}

