
use std::collections::HashMap;

use std::time::Duration;

use nwlab2::worker::hd::{Args, Spawn, WorkerExt};
use nwlab2::worker::hub::Hub;
use nwlab2::worker::Worker;

use nwlab2::router::worker::{GetNextHop, GetLinks,  SetupInterface};

struct TestFacade {
    hub: Hub,
    nodes: HashMap<String, Node>,
}

struct Node {
    namespace: Worker,
    router: Worker,
}

impl TestFacade {
    async fn create() -> Self {
        let hub = Hub::new().await.unwrap();

        Self {
            hub,
            nodes: Default::default(),
        }
    }

    async fn node(&mut self, name: &str) {
        let mut  namespace = self.hub.namespace().await.unwrap();
        let args = Args::from([assert_cmd::cargo::cargo_bin("node").to_str().
                              unwrap()])
            .args([name]);
        namespace.req(&Spawn{
            args,
            stdout: false,
            stderr: false
        }).await.unwrap();
        let router = self.hub.accept_worker().await.unwrap();
        self.nodes.insert(name.into(), Node { namespace, router });
    }

    async fn connect(&mut self, a: &str, a_if: &str, b_if: &str, b: &str) {
        let mut node_a = self.nodes.remove(a).unwrap();
        let mut node_b = self.nodes.remove(b).unwrap();

        node_a.namespace.connect(a_if, b_if, &mut node_b.namespace).await.unwrap();
        node_a.namespace.sh(format!("ip link set {a_if} up")).await.unwrap();
        node_b.namespace.sh(format!("ip link set {b_if} up")).await.unwrap();

        node_a.router.req(&SetupInterface{
            name: a_if.into(),
        }).await.unwrap();

        node_b.router.req(&SetupInterface{
            name: b_if.into(),
        }).await.unwrap();

        self.nodes.insert(a.into(), node_a);
        self.nodes.insert(b.into(), node_b);
    }

    async fn bridge(&mut self, node: &str, ifaces: impl IntoIterator<Item = &str>) {
        let node = self.nodes.get_mut(node).unwrap();

        node.namespace.sh("ip link add br0 type bridge").await.unwrap();
        for iface in ifaces {
            node.namespace.sh(format!("ip link set {iface} master br0"))
                .await.unwrap();
        }
        node.namespace.sh("ip link set br0 up").await.unwrap();
    }

    async fn get_route_ifname(&mut self, from: &str, to: &str) -> String {
        let node = self.nodes.get_mut(from).unwrap();
        let result = node.router.req(&GetNextHop {
            name: to.into(),
        }).await.unwrap();

        result.iface
    }

    async fn get_links(&mut self, from: &str, to: &str) -> Vec<String> {
        let node = self.nodes.get_mut(from).unwrap();
        let resp = node.router.req(&GetLinks {
            name: to.into(),
        }).await.unwrap();

        let mut ifaces: Vec<String> = resp.into_iter().map(|x| x.iface).collect();

        ifaces.sort();

        ifaces
    }
}

#[tokio::test]
async fn single_peer_over_single_link() {
    let mut f = TestFacade::create().await;

    f.node("A").await;
    f.node("B").await;
    f.connect("A", "eth0", "eth0", "B").await;
    tokio::time::sleep(Duration::from_secs(10)).await;
    assert_eq!(f.get_route_ifname("A", "B").await, "eth0");
}

#[tokio::test]
async fn single_peer_over_double_link() {
    let mut f = TestFacade::create().await;

    f.node("A").await;
    f.node("B").await;
    f.connect("A", "eth0", "eth0", "B").await;
    f.connect("A", "eth1", "eth1", "B").await;
    tokio::time::sleep(Duration::from_secs(10)).await;
    assert_eq!(f.get_links("A", "B").await, ["eth0", "eth1"]);
}

#[tokio::test]
async fn single_peer_over_double_link_and_switch() {
    let mut f = TestFacade::create().await;

    f.node("A").await;
    f.node("B").await;
    f.node("C").await;
    f.connect("A", "eth0", "eth0", "C").await;
    f.connect("A", "eth1", "eth1", "C").await;
    f.connect("C", "eth2", "eth0", "B").await;
    f.connect("C", "eth3", "eth1", "B").await;
    f.bridge("C", ["eth0", "eth1", "eth2", "eth3"]).await;
    tokio::time::sleep(Duration::from_secs(10)).await;
    assert_eq!(f.get_links("A", "B").await, ["eth0", "eth0", "eth1", "eth1"]);
}


