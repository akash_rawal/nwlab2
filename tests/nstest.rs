//Test namespace functionality
use nwlab2::worker::hub::*;
use nwlab2::worker::hd::*;

#[tokio::test]
async fn ping() {
    let mut hub = Hub::new().await.unwrap();

    let mut a = hub.namespace().await.unwrap();
    let mut b = hub.namespace().await.unwrap();

    //Connect two namespaces
    a.connect("eth0", "eth0", &mut b).await.unwrap();

    //Assign IP addresses
    a.sh("ip addr add 192.168.0.1/24 dev eth0").await.unwrap();
    a.sh("ip link set eth0 up").await.unwrap();
    b.sh("ip addr add 192.168.0.2/24 dev eth0").await.unwrap();
    b.sh("ip link set eth0 up").await.unwrap();

    //Ping test
    a.sh("ping -c 1 192.168.0.2").await.unwrap();
}

#[tokio::test]
async fn routed_ping() {
    let mut hub = Hub::new().await.unwrap();

    let mut a = hub.namespace().await.unwrap();
    let mut b = hub.namespace().await.unwrap();
    let mut router = hub.namespace().await.unwrap();


    //Connect namespaces to router
    router.connect("eth0", "eth0", &mut a).await.unwrap();
    router.connect("eth1", "eth0", &mut b).await.unwrap();

    //Assign IP addresses
    for (i, ns) in [(0, &mut a), (1, &mut b)] {
        //Router side
        router.sh(format!("ip addr add 192.168.{i}.1/24 dev eth{i}"))
            .await.unwrap();
        router.sh(format!("ip link set eth{i} up")).await.unwrap();

        //Namespace side
        ns.sh(format!("ip addr add 192.168.{i}.2/24 dev eth0")).await.unwrap();
        ns.sh("ip link set eth0 up").await.unwrap();
        ns.sh(format!("ip route add default via 192.168.{i}.1 dev eth0"))
            .await.unwrap();
    }


    a.sh("ping -c 1 192.168.1.2").await.unwrap();
}

